//(function ($) {
$(document).ready(function(){
	get_tweets();
	setInterval( "get_tweets()", Drupal.settings.live_tweets.interval );
});

function replaceURLWithHTMLLinks(text) {
	var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
	return text.replace(exp,"<a href='$1'>$1</a>"); 
}

String.prototype.parseURL = function() {
	return this.replace(/[A-Za-z]+:\/\/[A-Za-z0-9-_]+\.[A-Za-z0-9-_:%&~\?\/.=]+/g, function(url) {
		return url.link(url);
	});
};

String.prototype.parseUsername = function() {
	return this.replace(/[@]+[A-Za-z0-9-_]+/g, function(u) {
		var username = u.replace("@","")
		return u.link("http://twitter.com/"+username);
	});
};

String.prototype.parseHashtag = function() {
	return this.replace(/[#]+[A-Za-z0-9-_]+/g, function(t) {
		var tag = t.replace("#","%23")
		return t.link("http://search.twitter.com/search?q="+tag);
	});
};

function get_tweets() {

	var cur = new Date().getTime();

	$.getJSON('http://search.twitter.com/search.json?q='+ Drupal.settings.live_tweets.keyword +'&count=2&callback=?', function(data) {
			var html = '';
			value = data.results[0];
			if(value != null)
			{
				var tweet_link = livetweets_get_tweet_link( value.from_user, value.id_str );
				var tweet_date = livetweets_get_tweet_date( cur, value.created_at );
				var user_link = livetweets_get_user_link( value.from_user );

				html += '<div class="tweet last">';
				html += '<p class="text">'+ value.text.parseURL().parseUsername().parseHashtag() +'</p>';
				html += '<p class="tweet-links">';
				if(Drupal.settings.live_tweets.account != '')
	{
		html += '<span>follow <a href="http://twitter.com/'+Drupal.settings.live_tweets.account+'" target="__blank">@'+Drupal.settings.live_tweets.account+'</a></span>';
	}
	html += '<span class="date"><a href="'+ tweet_link +'" target="__blank">'+ tweet_date +'</a></span>';
	html += '<span class="avatar"><a href="'+ user_link +'" target="__blank"><img src="' + value.profile_image_url +'" alt="twitter-avatar" width="25" height="25" /></a></span>';

	html += '</p><div class="clear"></div></div>';
	html += '</div>';
	$('#livetweets .content').html(html);
			} else {
				$('#livetweets .content').html('No recent tweets found.');
			}
});
}

function livetweets_get_tweet_date( current, tweet_date ) {
	// Timestamp. Very simple, works well if popular subject
	//
	var months = new Array(12);
	months[0] = 'jan';
	months[1] = 'feb';
	months[2] = 'mar';
	months[3] = 'apr';
	months[4] = 'may';
	months[5] = 'jun';
	months[6] = 'jul';
	months[7] = 'aug';
	months[8] = 'sep';
	months[9] = 'oct';
	months[10] = 'nov';
	months[11] = 'dec';

	var diff_date = current - new Date(tweet_date);
	var seconds = Math.round( diff_date / 1000 );
	// return seconds;
	if( seconds < 60 ) {
		return  seconds + 's ago';
	} else if( seconds < 3600 ) {
		return Math.round( seconds / 60 ) + 'min ago';
	} else if( seconds < 86400 ) {
		return Math.round( seconds / 3600 ) + 'h ago';
	} else {
		var date = new Date(tweet_date);
		var month = date.getUTCMonth(); 
		return date.getUTCDate() + ' ' + months[month]; 
	}
}

function livetweets_get_tweet_link( user, tweet_id ) {
	var html = 'http://twitter.com/#!/' + user + '/status/' + tweet_id;
	return html;
}

function livetweets_get_user_link( user ) {
	var html = 'http://twitter.com/#!/' + user;
	return html;
}

